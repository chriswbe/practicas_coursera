
$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 2000
    });
    $('#staticBackdropModal').on('show.bs.modal',function(e){
        console.log("show.bs.modal")
        $('#contactoBtn').removeClass("btn-success")
        $('#contactoBtn').addClass("btn-primary")
        $('#contactoBtn').prop("disabled", true)
    })
    $('#staticBackdropModal').on('shown.bs.modal',function(e){
        console.log("shown.bs.modal")
    })
    $('#staticBackdropModal').on('hide.bs.modal',function(e){
        console.log("hide.bs.modal")
        $('#contactoBtn').prop("disabled", false)
    })
    $('#staticBackdropModal').on('hidden.bs.modal',function(e){
        console.log("hidden.bs.modal")
    })
    $('#staticBackdropModal').on('hidePrevented.bs.modal',function(e){
        console.log("hidePrevented.bs.modal")
    })
})
