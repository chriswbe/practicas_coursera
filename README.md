# Prácticas Coursera
El siguiente repositorio contiene las practicas realizadas en cursos coursera, las cuales estarán en sus respectivas ramas "branch"

#### Diseño páginas web con bootstrap4
- Semana 1: **semana1** - Presentación, introducción a bootstrap, diseño responsive y sistema de grillas, flex
- Semana 2: **semana2** - Navegabilidad, Breadcroumbs (jerarquía de ubicación), Formularios, Botones, Tablas, Tarjetas, Imágenes, Iconos, Notificaciones
- Semana 3: **semana3** - Componentes JS, Pestañas y navegacion por pestañas (pills, tabs), Visibilidad (collapse, accordions), Carroussel, Desplegando contenido (modals, tooltips, popov)
- Semana 4: **semana4** - JQuery, Less y Sass, Scripts en NPM, Grunt y Gulp